﻿using MagnolienRealty.DAL;
using MagnolienRealty.DAL.Repositories;
using MagnolienRealty.Entity;
using System.Collections.Generic;
using System.Linq;

namespace MagnolienRealty.BLL
{
    public class PhotoService
    {
        private readonly IRepository<PhotoAttachment> _photoRepository;

        public PhotoService()
        {
            _photoRepository = new PhotoRepository();
        }

        public PhotoService(IRepository<PhotoAttachment> repository)
        {
            _photoRepository = repository;
        }

        public IEnumerable<PhotoAttachment> GetAll()
        {
            var data = _photoRepository.GetAll();
            return data;
        }

        public PhotoAttachment GetById(int id)
        {
            var data = _photoRepository.GetById(id);
            return data;
        }

        public IEnumerable<PhotoAttachment> GetByRealtyId(int id)
        {
            var data = _photoRepository.GetAll().Where(x => x.RealtyId == id);
            return data;
        }

        public void Add(PhotoAttachment data)
        {
            _photoRepository.Add(data);
        }

        public void Update(PhotoAttachment data)
        {
            _photoRepository.Update(data);
        }

        public void Delete(int id)
        {
            var data = _photoRepository.GetById(id);
            _photoRepository.Delete(data);
        }

        public void DeleteByRealtyId(int id)
        {
            var data = GetByRealtyId(id);
            foreach(var photo in data)
            {
                _photoRepository.Delete(photo);
            }
        }
    }
}
