﻿using MagnolienRealty.DAL;
using MagnolienRealty.DAL.Repositories;
using MagnolienRealty.Entity;
using System.Collections.Generic;

namespace MagnolienRealty.BLL
{
    public class AdminService
    {
        private readonly IRepository<Admin> _adminRepository;

        public AdminService()
        {
            _adminRepository = new AdminRepository();
        }

        public AdminService(IRepository<Admin> repo)
        {
            _adminRepository = repo;
        }

        public IEnumerable<Admin> GetAll()
        {
            var data = _adminRepository.GetAll();
            return data;
        }

        public Admin GetById(int id)
        {
            var data = _adminRepository.GetById(id);
            return data;
        }

        public void Add(Admin admin)
        {
            _adminRepository.Add(admin);
        }

        public void Update(Admin admin)
        {
            _adminRepository.Update(admin);
        }

        public void Delete(int id)
        {
            _adminRepository.Delete(_adminRepository.GetById(id));
        }
    }
}
