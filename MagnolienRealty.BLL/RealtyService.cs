﻿using MagnolienRealty.DAL;
using MagnolienRealty.DAL.Repositories;
using MagnolienRealty.Entity;
using System.Collections.Generic;
using System.Linq;

namespace MagnolienRealty.BLL
{
    public class RealtyService
    {
        private readonly IRepository<RealtyForSale> _realtyRepository;

        public RealtyService()
        {
            _realtyRepository = new RealtyRepository();
        }
        public RealtyService(IRepository<RealtyForSale> repository)
        {
            _realtyRepository = repository;
        }

        public IEnumerable<RealtyForSale> GetAll()
        {
            var data = _realtyRepository.GetAll();
            return data;
        }

        public RealtyForSale GetById(int id)
        {
            var data = _realtyRepository.GetById(id);
            return data;
        }

        public IEnumerable<RealtyForSale> SimpleSearch(string searchString)
        {
            var data = new List<RealtyForSale>();
            data.AddRange(_realtyRepository.GetAll().Where(r => r.Address.ToLower().Contains(searchString.ToLower())));
            data.AddRange(_realtyRepository.GetAll().Where(r => r.Comments.ToLower().Contains(searchString.ToLower())));
            data.AddRange(_realtyRepository.GetAll().Where(r => r.Name.ToLower().Contains(searchString.ToLower())));

            data = data.Distinct().ToList();
            return data;
        }

        public void Add(RealtyForSale data)
        {
            _realtyRepository.Add(data);
        }

        public void Update(RealtyForSale data)
        {
            _realtyRepository.Update(data);
        }

        public void Delete(int id)
        {
            var data = GetById(id);
            _realtyRepository.Delete(data);
        }
    }
}
