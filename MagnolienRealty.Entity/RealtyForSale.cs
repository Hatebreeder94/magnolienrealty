﻿using System.ComponentModel.DataAnnotations;

namespace MagnolienRealty.Entity
{
    public class RealtyForSale
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RealtyType { get; set; }
        [Required]
        public double Area { get; set; }
        [Required]
        [MaxLength(300)]
        public string Address { get; set; }
        [Required]
        public bool ForRent { get; set; }
        [Required]
        [MaxLength(500)]
        public string Name { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Comments { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        [DataType(DataType.Currency)]
        public decimal PricePerMonth { get; set; }
        [Required]
        public string CurrencyType { get; set; }
        public bool IsFlat { get; set; }
        public int Level { get; set; }
        public int MaxLevel { get; set; }
        public int RoomQty { get; set; }
        public int ModeratorId { get; set; }
        public bool Approved { get; set; }
    }
}
