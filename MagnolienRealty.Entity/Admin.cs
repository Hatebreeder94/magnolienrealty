﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MagnolienRealty.Entity
{
    public class Admin
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public bool IsAdmin { get; set; }
        public int ApprovedAnnouncementsCount { get; set; }
        public DateTime LastApprovalTime { get; set; } 
    }
}
