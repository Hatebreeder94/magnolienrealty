﻿using System.ComponentModel.DataAnnotations;

namespace MagnolienRealty.Entity
{
    public class PhotoAttachment
    {
        [Key]
        public int id { get; set; }
        public int RealtyId { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
    }
}
