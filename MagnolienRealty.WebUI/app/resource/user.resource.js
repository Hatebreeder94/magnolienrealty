﻿(function () {
    "use strict";

    angular.module("app").factory("userResource", function ($resource, appSettings) {
        return $resource(appSettings.apiPath + "Admin/", { id: "@id" },
        {
            'login': {
                method: "POST",
                url: appSettings.apiPath + "Admin/Login"
            },
            'add': {
                method: "POST",
                url: appSettings.apiPath + "Admin/Add"
            },
            'delete': { method: "DELETE" },
            'getAll': { method: "GET", isArray: true }
        });
    });

})();