﻿(function () {
    "use strict";

    angular.module("app").factory("photoResource", function ($resource, appSettings) {
        return $resource(appSettings.apiPath + "Photo/:id", { id: "@id" },
        {
            'query': { method: "GET", isArray: true },
            'save': { method: "POST" },
            'update': { method: 'PUT' }

        });
    });

})();