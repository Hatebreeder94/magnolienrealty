﻿(function () {
    "use strict";

    angular.module("app").factory("realtyResource", function ($resource, appSettings) {
        return $resource(appSettings.apiPath + "Realty/:id", { id: "@id" },
        {
            'query': { method: "GET", isArray: true },
            'get' : {method: "GET"},
            'save': { method: "POST" },
            'update': { method: 'PUT' },
            'simpleSearch': {
                method: 'GET',
                url: appSettings.apiPath + "Realty/SimpleSearch?searchString=:searchString",
                params: {searchString: "@searchString"},
                isArray: true
            },
            'advancedSearch': {
                method: "POST",
                url: appSettings.apiPath + "Realty/AdvancedSearch",
                isArray: true
            },
            'getLastId': {
                method: "GET",
                url: appSettings.apiPath + "Realty/GetLastId"
            },
            'approve': {
                method: "POST",
                url: appSettings.apiPath + "Realty/Approve"
            }

        });
    });

})();