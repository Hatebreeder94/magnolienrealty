﻿(function () {
    "use strict";

    angular.module("app")
        .controller("HomeController", ["$scope", "$state", "realtyService", "exchangeRates", function ($scope, $state, realtyService, exchangeRates) {
            
            var data = realtyService.getAll();
            $scope.realtyList = data;

            $scope.simpleSearch = function (searchString) {
                var data = realtyService.simpleSearch(searchString);
                $scope.realtyList = data;
            };

            $scope.viewDetails = function (id) {
                $state.go("details", { "id": id });
            };

            $scope.setCurrencyType = function (currencyType) {
                if (currencyType == "dollar") {
                    return "$";
                } else if (currencyType == "euro") {
                    return "€";
                } else {
                    return "MDL";
                }
            };

            $scope.formatText = function (stringToFormat, maxValue) {
                if (stringToFormat.length > maxValue) {
                    var newString = stringToFormat.slice(0, maxValue - 1);
                    return newString + "...";
                } else if (stringToFormat.length <= maxValue && stringToFormat.length > 0) {
                    return stringToFormat;
                } else {
                    return " ";
                }
            };

            $scope.getExchangeRates = function () {
                var rates = exchangeRates.get();

                return rates[0];
            };


        }]);
}());