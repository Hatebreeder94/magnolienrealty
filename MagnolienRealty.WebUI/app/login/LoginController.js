﻿(function () {
    "use strict";

    angular.module("app")
        .controller("LoginController", ["$scope", "$state", "userService", function ($scope, $state, userService) {

            $scope.doLogin = function () {
                var userCredentials = {
                    Login: $scope.login,
                    Password: $scope.password
                };

                var userInfo = {};

                var returnData = userService.login(userCredentials);

                if (userService.getLoggedUser() != undefined) {
                    $scope.message = "Login Successful!";
                    
                    setTimeout(function () {
                        $state.go("index");
                    }, 1000);                    
                } else {
                    $scope.message = "Incorrect Login or Password"
                }
            };
        }]);
}());