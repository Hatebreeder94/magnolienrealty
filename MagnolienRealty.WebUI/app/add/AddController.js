﻿(function () {
    "use strict";

    angular.module("app")
        .controller("AddController", ["$scope", "$state", "realtyService", "attachmentService", function ($scope, $state, realtyService, attachmentService) {
            $scope.forRentScope = false;
            $scope.isFlatBool = true;
            $scope.RealtyType = "Private";
            $scope.ForRent = "false";
            $scope.CurrencyType = "euro";
            $scope.IsFlat = "true";

            $scope.captchaA = Math.ceil(Math.random() * 10);
            $scope.captchaB = Math.ceil(Math.random() * 10);
            var captchaAnswer = $scope.captchaA + $scope.captchaB;

            realtyService.getLastId(function (result) {
                $scope.id = result;
            });



            $scope.forRentValueFormat = function () {
                var selectValue = $("select[name='ForRent']").val();

                if (selectValue == "true") {
                    $scope.forRentScope = true;
                } else {
                    $scope.forRentScope = false;
                };
            };

            $scope.onBuildingTypeChange = function () {
                var selectValue = $("select[name='IsFlat']").val();
                var buildingTypeSelect = $("div#buildingTypeSelect");

                if (selectValue == "true") {
                    $scope.isFlatBool = true;
                    buildingTypeSelect.removeClass("col-md-5").addClass("col-md-3");
                } else {
                    $scope.isFlatBool = false;
                    buildingTypeSelect.removeClass("col-md-3").addClass("col-md-5");
                }
            };


            var validateCaptcha = function (response) {
                if (captchaAnswer == response) {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.submitForm = function () {

                if (!validateCaptcha($scope.captchaResponse)) {
                    showCaptchavalidationError(true);
                    return;
                } else {
                    showCaptchavalidationError(false);
                }

                var dataToSend = {
                    RealtyType: $scope.RealtyType,
                    ForRent: $scope.forRentScope,
                    Area: $scope.Area,
                    Price: $scope.Price,
                    PricePerMonth: $scope.PricePerMonth,
                    CurrencyType: $scope.CurrencyType,
                    Address: $scope.Address,
                    IsFlat: $scope.isFlatBool,
                    Level: $scope.Level,
                    MaxLevel: $scope.MaxLevel,
                    RoomQty: $scope.RoomQty,
                    Name: $scope.Name,
                    Comments: $scope.Comments,

                };

                realtyService.Add(dataToSend);

                $state.go("index");
            };

            var showCaptchavalidationError = function (param) {
                if (param) {
                    $("#captchaValidationError").show();
                } else {
                    $("#captchaValidationError").hide();
                }

            };


            $scope.getFileDetails = function (e) {

                $scope.files = [];
                $scope.$apply(function () {

                    // STORE THE FILE OBJECT IN AN ARRAY.
                    for (var i = 0; i < e.files.length; i++) {
                        $scope.files.push(e.files[i])
                    }

                });
            };

            $scope.uploadFiles = function () {
                attachmentService.uploadFiles($scope.files, $scope.id);
            };
        }]);
}());