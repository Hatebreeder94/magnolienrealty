﻿(function () {
    "use strict";

    angular.module("app")
        .controller("SearchController", ["$scope", "$state","realtyService", "exchangeRates", function ($scope, $state, realtyService, exchangeRates) {

            $scope.searchViewModel = {
                RealtyType: "",
                ForRent: "",
                AreaMin: "",
                AreaMax: "",
                PriceMinEUR: "",
                PriceMaxEUR: "",
                PriceMinUSD: "",
                PriceMaxUSD: "",
                PriceMinMDL: "",
                PriceMaxMDL: "",
                CurrencyType: "reset",
                Address: "",
                BuildingType: "",
                LevelMin: "",
                LevelMax: "",
                RoomsMin: "",
                RoomsMax: ""
            };

            
            $scope.onChangeMin = function (id) {
                var minElement = $("#" + id + "Min");
                var maxElement = $("#" + id + "Max");
                var minValue = minElement.val();
                var maxValue = maxElement.val();

                if (maxValue === "") maxValue = "0";

                if (parseInt(maxValue) <= parseInt(minValue)) {
                    $scope.searchViewModel.PriceMax = parseInt(minElement.val()) + 1;
                    //maxElement.val(parseInt(minElement.val()) + 1);
                };
            };
            
            $scope.onChangeMax = function (id) {
                var minElement = $("#" + id + "Min");
                var maxElement = $("#" + id + "Max");
                var debug = minElement.val();
                var debug2 = maxElement.val();

                if (minElement.val() == "") {
                    $scope.searchViewModel.PriceMin = 0;
                    //minElement.val(0);
                }
            };
           


            $scope.doSearch = function (searchObject) {

                var rates = {};

                exchangeRates.get(function (result) {
                    $scope.rates = result.rates;

                    if (searchObject.CurrencyType == "euro") {
                        searchObject.PriceMinEUR = searchObject.PriceMin;
                        searchObject.PriceMaxEUR = searchObject.PriceMax;
                        searchObject.PriceMinUSD = searchObject.PriceMin / $scope.rates.EUR;
                        searchObject.PriceMaxUSD = searchObject.PriceMax / $scope.rates.EUR;
                        searchObject.PriceMinMDL = searchObject.PriceMinUSD * $scope.rates.MDL;
                        searchObject.PriceMaxMDL = searchObject.PriceMaxUSD * $scope.rates.MDL;
                    } else if (searchObject.CurrencyType == "dollar") {
                        searchObject.PriceMinUSD = searchObject.PriceMin;
                        searchObject.PriceMaxUSD = searchObject.PriceMax;
                        searchObject.PriceMinEUR = searchObject.PriceMin * $scope.rates.EUR;
                        searchObject.PriceMaxEUR = searchObject.PriceMax * $scope.rates.EUR;
                        searchObject.PriceMinMDL = searchObject.PriceMin * $scope.rates.MDL;
                        searchObject.PriceMaxMDL = searchObject.PriceMax * $scope.rates.MDL;
                    } else if (searchObject.CurrencyType == "MDL") {
                        searchObject.PriceMinMDL = searchObject.PriceMin;
                        searchObject.PriceMaxMDL = searchObject.PriceMax;
                        searchObject.PriceMinEUR = searchObject.PriceMin / $scope.rates.MDL * $scope.rates.EUR;
                        searchObject.PriceMaxEUR = searchObject.PriceMax / $scope.rates.MDL * $scope.rates.EUR;
                        searchObject.PriceMinUSD = searchObject.PriceMinEUR / $scope.rates.EUR;
                        searchObject.PriceMaxUSD = searchObject.PriceMaxEUR / $scope.rates.EUR;
                    }

                    realtyService.advancedSearch(searchObject, function (result) {
                        $scope.realtyList = result;
                    });
                });
            };

            $scope.viewDetails = function (id) {
                $state.go("advancedSearch.details", { "id": id });
            };

            $scope.setCurrencyType = function (currencyType) {
                if (currencyType == "dollar") {
                    return "$";
                } else if (currencyType == "euro") {
                    return "€";
                } else {
                    return "MDL";
                }
            };

            $scope.formatText = function (stringToFormat, maxValue) {
                if (stringToFormat.length > maxValue) {
                    var newString = stringToFormat.slice(0, maxValue - 1);
                    return newString + "...";
                } else if (stringToFormat.length <= maxValue && stringToFormat.length > 0) {
                    return stringToFormat;
                } else {
                    return " ";
                }
            };

            $scope.resetPrice = function () {
                if ($scope.searchViewModel.CurrencyType === "reset") {
                    $scope.searchViewModel.PriceMin = "";
                    $scope.searchViewModel.PriceMax = "";
                    $scope.searchViewModel.PriceMinEUR = "";
                    $scope.searchViewModel.PriceMinUSD = "";
                    $scope.searchViewModel.PriceMinMDL = "";
                    $scope.searchViewModel.PriceMaxEUR = "";
                    $scope.searchViewModel.PriceMaxUSD = "";
                    $scope.searchViewModel.PriceMaxMDL = "";
                }
            };

        }]);
}());