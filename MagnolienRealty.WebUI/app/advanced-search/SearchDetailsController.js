﻿(function () {
    "use strict";

    angular.module("app")
        .controller("SearchDetailsController", ["$scope", "$stateParams", "realtyService", function ($scope, $stateParams, realtyService) {
            var realty = realtyService.getById($stateParams.id);
            $scope.data = realty;


        }]);
}());