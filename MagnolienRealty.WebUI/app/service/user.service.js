﻿(function () {
    "use strict";

    angular.module("app")
        .factory("userService", function ($http, $cookies, userResource, appSettings) {
            var _userService = {};

            _userService.getLoggedUser = function () {
                return $cookies.getObject('loggedUser');
            };

            _userService.login = function (userCredentials, callback) {
                $('body').loader("show");
                return userResource.login(userCredentials, function (result) {
                    $('body').loader("hide");
                    $cookies.putObject("loggedUser", result)
                    callback(result);
                    return result ;
                }, function () {
                    $("body").loader('hide');
                    callback();
                });
            };

            _userService.addUser = function (user) {
                $('body').loader("show");
                userResource.add(user);
                $('body').loader("hide");
            };

            _userService.delete = function (id) {
                $('body').loader("show");
                userResource.delete({ id: id });
                $('body').loader("hide");
            };

            _userService.getAll = function () {
                $('body').loader("show");
                return userResource.getAll(function (result) {
                    $('body').loader("hide");
                    return typeof result == "object" ? result : false;
                });
            };

            _userService.logout = function () {
                $cookies.remove('loggedUser');
            };

            _userService.incrementApprovedCount = function (id) {
                $('body').loader('show');
                $http.post(appSettings.apiPath + "Admin/IncrementApprovalCount/" + id)
                    .then(function () {
                        $('body').loader("hide");
                    }, function () {
                        $('body').loader("hide");
                        alert("couldn't increment");
                    });
            };

            return _userService;
        });
}());