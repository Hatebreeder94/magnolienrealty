﻿(function () {


    angular.module("app")
        .factory("realtyService", function (realtyResource, appSettings) {
		    var _realtyService = {};

		    _realtyService.getAll = function () {
		        $('body').loader("show");
		        return realtyResource.query(function (result) {
		            $('body').loader('hide');
		            return result;
		        });

		    };

		    _realtyService.getLastId = function (callback) {
		        return realtyResource.getLastId(function (result) {
		            callback(result);
		        }, function () {
		            alert("couldn't get last id");
		        });

		    };

		    _realtyService.getById = function (id) {
		        return realtyResource.get({ id: id }, function (result) {
		            return result;
		        });
		    };

		    _realtyService.Add = function (dataToSend) {
		        $('body').loader('show');
		        realtyResource.save(dataToSend);
		        $('body').loader('hide')
		    };

		    _realtyService.simpleSearch = function (searchString) {
		        return realtyResource.simpleSearch({ searchString: searchString }, function (result) {
		            return result;
		        });
		    };

		    _realtyService.advancedSearch = function (searchObject, callback) {
		        $('body').loader("show");
		        realtyResource.advancedSearch(searchObject, function (result) {
		            $('body').loader("hide");
		            callback(result);
		        }, function () {
		            $('body').loader("hide");
		            alert("error");
		        });
		    };

		    _realtyService.approve = function (data) {
		        $('body').loader("show");
		        realtyResource.approve(data, function () {
		            $('body').loader('hide');
		        }, function () {
		            $('body').loader('hide');
		            alert("couldn't approve");
		        })
		    };

		    return _realtyService;
	    });
}());