﻿(function () {
    "use strict";

    angular.module("app")
        .factory("attachmentService", function (photoResource, appSettings) {
            var _attachmentService = {};

            _attachmentService.getAll = function () {
                $("body").loader("show");
                return photoResource.query(function (result) {
                    $("body").loader('hide');
                    return result;
                });
            };

            _attachmentService.getByRealtyId = function (id) {
                $("body").loader("show");
                return photoResource.query({ id: id }, function (result) {
                    $("body").loader('hide');
                    return result;
                });

            };

            _attachmentService.uploadFiles = function (dataToSend, id) {

                function updateProgress(e) {
                    if (e.lengthComputable) {
                        document.getElementById('pro').setAttribute('value', e.loaded);
                        document.getElementById('pro').setAttribute('max', e.total);
                    }
                }

                // CONFIRMATION.
                function transferComplete(e) {
                    alert("Files uploaded successfully.");
                }

                var data = new FormData();

                for (var i in dataToSend) {
                    data.append("uploadedFile", dataToSend[i]);
                }

                // ADD LISTENERS.
                var objXhr = new XMLHttpRequest();
                objXhr.addEventListener("progress", updateProgress, false);
                objXhr.addEventListener("load", transferComplete, false);

                // SEND FILE DETAILS TO THE API.
                objXhr.open("POST", appSettings.apiPath +  "Photo/UploadPhoto/" + id);
                objXhr.send(data);

                
            };

            return _attachmentService;
        });
}());