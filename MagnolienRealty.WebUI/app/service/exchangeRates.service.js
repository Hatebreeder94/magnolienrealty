﻿(function () {
    "use strict";

    angular.module("app").factory("exchangeRates", function ($http, appSettings) {
        var _exchangeRates = {};

        var appId = "6a0c9e81007c4fdeac6197c3d1eee7b7";

        _exchangeRates.get = function (callback) {
            $http.get("https://openexchangerates.org/api/latest.json?app_id=" + appSettings.exchangeRatesAppId)
                        .then(function (response) {
                            callback(response.data);
                        }, function () {
                            alert("couldn't load exchange rates");
                        });
        };

        return _exchangeRates;
    });
}());