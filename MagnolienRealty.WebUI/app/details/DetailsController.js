﻿(function () {
    "use strict";

    angular.module("app")
        .controller("DetailsController", ["$scope", "$stateParams", "realtyService", "attachmentService", "userService", "exchangeRates", function ($scope, $stateParams, realtyService, attachmentService, userService, exchangeRates) {
            var realty = realtyService.getById($stateParams.id);
            $scope.data = realty;

            $scope.setCurrencyType = function (currencyType) {
                if (currencyType == "dollar") {

                    return "$";
                } else if (currencyType == "euro") {

                    return "€";
                } else {

                    return "MDL";
                }
            };

            

            var attachmentsArr = attachmentService.getByRealtyId($stateParams.id);
            $scope.attachments = attachmentsArr;

        }]);
}());