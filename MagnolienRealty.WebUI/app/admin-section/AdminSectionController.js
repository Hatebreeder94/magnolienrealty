﻿(function () {
    "use strict";

    angular.module("app")
            .controller("AdminSectionController", ["$scope", "$state", "userService", function ($scope, $state,userService) {
                var loggedUser = userService.getLoggedUser();
                if (loggedUser == undefined || !loggedUser.IsAdmin) {
                    $state.go("login");
                }

                $scope.getUserList = function () {
                    $scope.userList = userService.getAll();
                };

                $scope.getUserList();                

                $scope.addUser = function (userObject) {
                    userService.addUser(userObject);
                    $scope.getUserList();
                    $('#user-list.admin-tab').addClass("active");
                    $('#add-user.admin-tab').removeClass("active");
                    $state.go("admin-section.userList");
                };

                $scope.deleteUser = function (id) {
                    userService.delete(id);
                    $scope.getUserList();
                };

                $(".admin-tab").each(function () {
                    $(this).click(function () {
                        $(".admin-tab").each(function () {
                            $(this).removeClass('active')
                        });
                        $(this).addClass('active');
                    });
                });


            }]);
}());