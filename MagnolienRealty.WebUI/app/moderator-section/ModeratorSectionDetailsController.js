﻿(function () {
    "use strict";

    angular.module("app")
        .controller("ModeratorSectionDetailsController", ["$scope", "$stateParams", "realtyService", "userService", function ($scope, $stateParams, realtyService, userService) {
            var realty = realtyService.getById($stateParams.id);
            $scope.data = realty;
            var loggedUser = userService.getLoggedUser();

            $scope.approveAnnouncement = function (id) {
                var data = {
                    "RealtyId": id,
                    "ModeratorId": loggedUser.Id
                };

                $scope.data.Approved = true;

                realtyService.approve(data);
                userService.incrementApprovedCount(data.ModeratorId);
            };
        }]);
}());