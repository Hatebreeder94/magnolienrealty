﻿(function () {
    "use strict";

    angular.module("app")
            .controller("ModeratorSectionController", ["$scope", "$state", "realtyService", "userService", function ($scope, $state, realtyService, userService) {
                var loggedUser = userService.getLoggedUser();
                if (loggedUser == undefined) {
                    $state.go("login");
                }

                var data = realtyService.getAll();
                $scope.realtyList = data;

                

                $scope.setCurrencyType = function (currencyType) {
                    if (currencyType == "dollar") {
                        return "$";
                    } else if (currencyType == "euro") {
                        return "€";
                    } else {
                        return "MDL";
                    }
                };

                $scope.formatText = function (stringToFormat, maxValue) {
                    if (stringToFormat.length > maxValue) {
                        var newString = stringToFormat.slice(0, maxValue - 1);
                        return newString + "...";
                    } else if (stringToFormat.length <= maxValue && stringToFormat.length > 0) {
                        return stringToFormat;
                    } else {
                        return " ";
                    }
                };

                $scope.viewDetails = function (id) {
                    $state.go("moderator-section.details", { "id": id });
                };

                $scope.approveAnnouncement = function (id) {
                    var data = {
                        "RealtyId": id,
                        "ModeratorId": loggedUser.Id
                    };

                    realtyService.approve(data);
                    userService.incrementApprovedCount(data.ModeratorId);
                };
            }]);
}());