﻿(function () {
    "use strict";

    angular.module("app")
        .controller("UserStateController", ["$scope", "$rootScope", "$state", "userService", function ($scope, $rootScope,$state, userService) {

            $rootScope.loggedUser = userService.getLoggedUser();

            $scope.init = function () {
                $scope.getLoggedUser();
            }

            $scope.getLoggedUser = function () {
                $rootScope.loggedUser = userService.getLoggedUser();
            };

            $scope.doLogout = function () {
                userService.logout();
                $rootScope.loggedUser = {};
            };

            $scope.doLogin = function () {
                var userCredentials = {
                    Login: $scope.login,
                    Password: $scope.password
                };

                setTimeout(function () {
                }, 4000);

                userService.login(userCredentials, function (result) {
                    if (result != undefined) {
                        $scope.message = "Login Successful!";

                        $rootScope.loggedUser = result;

                        $state.go('index');
                    } else {
                        $scope.message = "Incorrect Login or Password";
                    }

                    
                });



                //if (user != undefined) {
                //    $scope.message = "Login Successful!";

                //    var user_logged = userService.getLoggedUser();

                //    $rootScope.loggedUser = user_logged;

                //    //$state.go("index");                    
                //} else {
                    
                //}
            };
        }]);
}());