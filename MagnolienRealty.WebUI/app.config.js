﻿(function () {
    "use strict";

    angular.module("app")
        .constant("appSettings", {
            "apiPath": "http://localhost:20123/api/",
            "exchangeRatesAppId": "6a0c9e81007c4fdeac6197c3d1eee7b7"
        })
        .config(["$urlRouterProvider", "$stateProvider", function ($urlRouterProvider, $stateProvider) {
            $urlRouterProvider.otherwise("/");
            $stateProvider
                .state("index", {
                    url: "/",
                    templateUrl: "app/home/home.html",
                    controller: "HomeController"
                })
                .state("login", {
                    url: "/login",
                    templateUrl: "app/login/login.html",
                    controller: "UserStateController"
                })
                .state("admin-section", {
                    url: "/admin",
                    templateUrl: "app/admin-section/admin-section.html",
                    controller: "AdminSectionController"
                })
                .state("admin-section.userList", {
                    url: "/list",
                    templateUrl: "app/admin-section/admin-section-user-list.html",
                    controller: "AdminSectionController"
                })
                .state("admin-section.add", {
                    url: "/add",
                    templateUrl: "app/admin-section/admin-section-moderator-add.html",
                    controller: "AdminSectionController"
                })
                .state("moderator-section", {
                    url: "/moderator",
                    templateUrl: "app/moderator-section/moderator-section.html",
                    controller: "ModeratorSectionController"
                })
                .state("moderator-section.details", {
                    url: "/moderator/details?:id",
                    templateUrl: "app/moderator-section/moderator-section-details.html",
                    controller: "ModeratorSectionDetailsController"
                })
                .state("add", {
                    url: "/add",
                    templateUrl: "app/add/add.html",
                    controller: "AddController"
                })
                .state("advancedSearch", {
                    url: "/advanced-search",
                    templateUrl: "app/advanced-search/advanced-search.html",
                    controller: "SearchController"
                })
                .state("advancedSearch.details", {
                    url: "/advanced-search/details?:id",
                    templateUrl: "app/advanced-search/advanced-search-details.html",
                    controller: "SearchDetailsController"
                })
                .state("details", {
                    url: "/details?:id",
                    templateUrl: "app/details/details.html",
                    controller: "DetailsController"
                });
        }]);
}());