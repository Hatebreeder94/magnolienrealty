﻿(function () {
    "use strict";

    angular.module("app", [
        "ngAnimate",
        "anim-in-out",
        "ngResource",
        "ui.router",
        "ngCookies"
    ]);
}());