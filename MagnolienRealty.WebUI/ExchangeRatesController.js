﻿(function () {
    "use strict";

    angular.module("app")
        .controller("ExchangeRatesController", ["$scope", "exchangeRates", function ($scope, exchangeRates) {
            var ratesPromise = exchangeRates.get();
            ratesPromise.then(function (result) {
                var ratesObj = result;

                $scope.USDtoMDL = ratesObj.rates.MDL;
                $scope.EURtoMDL = ratesObj.rates.MDL / ratesObj.rates.EUR;
                $scope.RONtoMDL = ratesObj.rates.MDL / ratesObj.rates.RON;
            })
        }]);
}());