﻿using MagnolienRealty.BLL;
using MagnolienRealty.Entity;
using MagnolienRealty.WebAPI.Models;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MagnolienRealty.WebAPI.Controllers
{
    public class PhotoController : ApiController
    {
        private PhotoService _photoService = new PhotoService();

        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            var data = _photoService.GetAll();
            if (data == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Returned NULL");
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("api/Photo/GetById/{id}")]
        public HttpResponseMessage GetById(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Id not specified");
            }
            var data = _photoService.GetById(id);
            if (data == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Returned NULL");
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("api/Photo/{id}")]
        public HttpResponseMessage GetByRealtyId(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Id not specified");
            }
            var data = _photoService.GetByRealtyId(id);
            if (data == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Returned NULL");
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpPost]
        public HttpResponseMessage Add([FromBody]PhotoAttachment data)
        {
            if (data == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Data to add came NULL");
            }
            _photoService.Add(data);
            return Request.CreateResponse(HttpStatusCode.OK, "Added to Database");
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No id provided for deletion");
            }
            _photoService.Delete(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted from Database");
        }

        [HttpDelete]
        [Route("api/Photo/DeleteByRealtyId/{id}")]
        public HttpResponseMessage DeleteByRealtyId(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No id provided for deletion");
            }
            _photoService.DeleteByRealtyId(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted all photos related to this Realty ID");
        }

        [HttpPost]
        [Route("api/Photo/UploadPhoto/{id}")]
        public async Task<HttpResponseMessage> UploadPhoto(int id)
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Content/attachments");
            var provider = new CustomMultipartFormDataStreamProvider(root);

            try
            {

                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    Trace.WriteLine(file.Headers.ContentDisposition.FileName);
                    Trace.WriteLine("Server file path: " + file.LocalFileName);

                    var startIndex = file.LocalFileName.LastIndexOf("\\") + 1;
                    var substringLength = file.LocalFileName.Length - file.LocalFileName.LastIndexOf("\\") - 1;

                    var fileName = file.LocalFileName.Substring(startIndex, substringLength);

                    var data = new PhotoAttachment
                    {
                        Name = fileName,
                        ContentType = "image/jpg",
                        RealtyId = id + 1,
                    };

                    _photoService.Add(data);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}