﻿using MagnolienRealty.BLL;
using MagnolienRealty.Entity;
using MagnolienRealty.WebAPI.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MagnolienRealty.WebAPI.Controllers
{
    public class RealtyController : ApiController
    {
        private RealtyService _realtyService = new RealtyService();
        
        [HttpGet]
        [Route("api/Realty")]
        public HttpResponseMessage GetAll()
        {
            var data = _realtyService.GetAll();
            if (data == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Returned NULL");
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("api/Realty/{id}")]
        public HttpResponseMessage GetById(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Id not specified");
            }
            var data = _realtyService.GetById(id);
            if (data == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Returned NULL");
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("api/Realty/SimpleSearch")]
        public HttpResponseMessage SimpleSearch(string searchString)
        {
            if (searchString == null)
            {
                var allRealty = _realtyService.GetAll();
                return Request.CreateResponse(HttpStatusCode.OK, allRealty);
            }

            var data = _realtyService.SimpleSearch(searchString);
            return Request.CreateResponse(HttpStatusCode.OK, data);            
        }

        [HttpPost]
        [Route("api/Realty/AdvancedSearch")]
        public HttpResponseMessage AdvancedSearch([FromBody] SearchViewModel model)
        {
            if (model == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "model empty");
            }

            var data = _realtyService.GetAll()
                                .Where
                                (
                                    x =>
                                        ((model.AreaMin != null && model.AreaMax != null) ? (x.Area >= model.AreaMin && x.Area <= model.AreaMax) : true) &&
                                        (model.RealtyType != "" ? (x.RealtyType == model.RealtyType) : true) &&
                                        (model.ForRent != null ? (x.ForRent == model.ForRent) : true) &&
                                        (model.Address != "" ? x.Address.ToLower().Contains(model.Address.ToLower()) : true) &&
                                        (model.CurrencyType != "" ?
                                            (
                                                x.CurrencyType == "euro" ?
                                                (
                                                    (x.Price >= Convert.ToDecimal(model.PriceMinEUR) && x.Price <= Convert.ToDecimal(model.PriceMaxEUR)) ||
                                                    (x.PricePerMonth >= Convert.ToDecimal(model.PriceMinEUR) && x.PricePerMonth <= Convert.ToDecimal(model.PriceMaxEUR))
                                                ) : 
                                                (x.CurrencyType == "dollar" ?
                                                (
                                                    (x.Price >= Convert.ToDecimal(model.PriceMinUSD) && x.Price <= Convert.ToDecimal(model.PriceMaxUSD)) ||
                                                    (x.PricePerMonth >= Convert.ToDecimal(model.PriceMinUSD) && x.PricePerMonth <= Convert.ToDecimal(model.PriceMaxUSD))
                                                ) :
                                                (x.CurrencyType == "MDL" ?
                                                (
                                                    (x.Price >= Convert.ToDecimal(model.PriceMinMDL) && x.Price <= Convert.ToDecimal(model.PriceMaxMDL)) ||
                                                    (x.PricePerMonth >= Convert.ToDecimal(model.PriceMinMDL) && x.PricePerMonth <= Convert.ToDecimal(model.PriceMaxMDL))
                                                ) : true))
                                            ) : true
                                        ) &&
                                        (x.Approved == true)
                                                
                                );

                                        

            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("api/Realty")]
        public HttpResponseMessage Add([FromBody]RealtyForSale data)
        {
            if (data == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Data to add came NULL");
            }
            _realtyService.Add(data);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No id provided for deletion");
            }
            _realtyService.Delete(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("api/Realty/GetLastId")]
        public HttpResponseMessage GetLastId()
        {
            var data = _realtyService.GetAll().Last().Id;
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("api/Realty/Approve")]
        public HttpResponseMessage Approve([FromBody]ApprovalViewModel model)
        {
            if (model == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No Ids specified");
            }

            var realtyToApprove = _realtyService.GetById(model.RealtyId);

            realtyToApprove.Approved = true;
            realtyToApprove.ModeratorId = model.ModeratorId;

            _realtyService.Update(realtyToApprove);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }
}
