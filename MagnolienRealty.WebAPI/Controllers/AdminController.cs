﻿using MagnolienRealty.BLL;
using MagnolienRealty.Entity;
using MagnolienRealty.WebAPI.Models;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace MagnolienRealty.WebAPI.Controllers
{
    public class AdminController : ApiController
    {
        private AdminService _adminService = new AdminService();

        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            var data = _adminService.GetAll();
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("api/Admin/Login")]
        public HttpResponseMessage Login([FromBody]LoginViewModel model)
        {
            var shaEncrypter = new SHA1CryptoServiceProvider();

            var passwordStream = new MemoryStream(Encoding.UTF8.GetBytes(model.Password));

            var encryptedPassword = shaEncrypter.ComputeHash(passwordStream);

            var userList = _adminService.GetAll();

            foreach (var user in userList)
            {
                if (user.Login == model.Login)
                {
                    if (user.PasswordHash == Encoding.UTF8.GetString(encryptedPassword))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, user);
                    }
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Incorrect Login or Password");
        }

        [HttpPost]
        [Route("api/Admin/Add")]
        public HttpResponseMessage Add([FromBody]RegisterViewModel model)
        {
            if (model == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "model came empty");
            }

            var shaEncrypter = new SHA1CryptoServiceProvider();
            var passwordStream = new MemoryStream(Encoding.UTF8.GetBytes(model.Password));
            var encryptedPassword = shaEncrypter.ComputeHash(passwordStream);

            var data = new Admin()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Login = model.Login,
                IsAdmin = model.IsAdmin,
                PasswordHash = Encoding.UTF8.GetString(encryptedPassword),
                LastApprovalTime = DateTime.Now,
                ApprovedAnnouncementsCount = 0
            };

            _adminService.Add(data);

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpPost]
        [Route("api/Admin/IncrementApprovalCount/{id}")]
        public HttpResponseMessage IncrementApprovalCount(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No id specified");
            }

            var user = _adminService.GetById(id);
            user.ApprovedAnnouncementsCount += 1;
            user.LastApprovalTime = DateTime.Now;
            _adminService.Update(user);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            if (id == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Id is null");
            }

            _adminService.Delete(id);

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
