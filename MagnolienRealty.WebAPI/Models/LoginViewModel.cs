﻿namespace MagnolienRealty.WebAPI.Models
{
    public class LoginViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}