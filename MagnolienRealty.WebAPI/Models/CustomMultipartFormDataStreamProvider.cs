﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MagnolienRealty.WebAPI.Models
{
    public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public CustomMultipartFormDataStreamProvider(string path) : base(path) { }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            return DateTime.Now.Ticks + ".jpg";
        }
    }
    
}