﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagnolienRealty.WebAPI.Models
{
    public class SearchViewModel
    {
        public string RealtyType { get; set; }
        public bool? ForRent { get; set; }
        public int? AreaMin { get; set; }
        public int? AreaMax { get; set; }
        public double? PriceMin { get; set; }
        public double? PriceMax { get; set; }
        public double? PriceMinEUR { get; set; }
        public double? PriceMaxEUR { get; set; }
        public double? PriceMinUSD { get; set; }
        public double? PriceMaxUSD { get; set; }
        public double? PriceMinMDL { get; set; }
        public double? PriceMaxMDL { get; set; }
        public string CurrencyType { get; set; }
        public string Address { get; set; }
        public string BuildingType { get; set; }
        public int? LevelMin { get; set; }
        public int? LevelMax { get; set; }
        public int? RoomsMin { get; set; }
        public int? RoomsMax { get; set; }
    }
}