﻿namespace MagnolienRealty.WebAPI.Models
{
    public class ApprovalViewModel
    {
        public int RealtyId { get; set; }
        public int ModeratorId { get; set; }

    }
}