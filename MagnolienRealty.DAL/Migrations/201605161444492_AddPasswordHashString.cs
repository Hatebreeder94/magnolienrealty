namespace MagnolienRealty.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPasswordHashString : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Admins", "PasswordHash", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Admins", "PasswordHash");
        }
    }
}
