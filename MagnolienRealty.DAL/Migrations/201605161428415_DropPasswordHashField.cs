namespace MagnolienRealty.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropPasswordHashField : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Admins", "PasswordHash");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Admins", "PasswordHash", c => c.String());
        }
    }
}
