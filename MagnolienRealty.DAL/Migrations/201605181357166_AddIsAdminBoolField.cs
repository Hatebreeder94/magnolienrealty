namespace MagnolienRealty.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsAdminBoolField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Admins", "IsAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Admins", "IsAdmin");
        }
    }
}
