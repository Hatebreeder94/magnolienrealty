namespace MagnolienRealty.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModeratorIdAndApprovalCountDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RealtyForSales", "ModeratorId", c => c.Int(nullable: false));
            DropColumn("dbo.RealtyForSales", "SellerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RealtyForSales", "SellerId", c => c.Int(nullable: false));
            DropColumn("dbo.RealtyForSales", "ModeratorId");
        }
    }
}
