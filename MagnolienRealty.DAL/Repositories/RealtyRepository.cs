﻿using MagnolienRealty.Entity;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace MagnolienRealty.DAL.Repositories
{
    public class RealtyRepository : IRepository<RealtyForSale>
    {
        private MagnolienRealtyDbContext _db = new MagnolienRealtyDbContext();

        public IEnumerable<RealtyForSale> GetAll()
        {
            var data = _db.Realties.ToList();
            return data;
        }

        public RealtyForSale GetById(int id)
        {
            var data = _db.Realties.FirstOrDefault(x => x.Id == id);
            return data;
        }

        public void Add(RealtyForSale data)
        {
            _db.Realties.Add(data);
            _db.SaveChanges();
        }
        public void Delete(RealtyForSale data)
        {
            _db.Realties.Remove(data);
            _db.SaveChanges();
        }

        public void Update(RealtyForSale data)
        {
            _db.Realties.AddOrUpdate(data);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
