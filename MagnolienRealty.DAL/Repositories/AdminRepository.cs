﻿using MagnolienRealty.Entity;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace MagnolienRealty.DAL.Repositories
{
    public class AdminRepository : IRepository<Admin>
    {
        private readonly MagnolienRealtyDbContext _db = new MagnolienRealtyDbContext();

        public IEnumerable<Admin> GetAll()
        {
            var data = _db.Admins.ToList();
            return data;
        }

        public Admin GetById(int id)
        {
            var data = _db.Admins.FirstOrDefault(x => x.Id == id);
            return data;
        }

        public void Add(Admin admin)
        {
            _db.Admins.Add(admin);
            _db.SaveChanges();
        }

        public void Delete(Admin admin)
        {
            _db.Admins.Remove(admin);
            _db.SaveChanges();
        }

        public void Update(Admin data)
        {
            _db.Admins.AddOrUpdate(data);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
