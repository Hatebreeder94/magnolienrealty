﻿using MagnolienRealty.Entity;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace MagnolienRealty.DAL.Repositories
{
    public class PhotoRepository : IRepository<PhotoAttachment>
    {
        private MagnolienRealtyDbContext _db = new MagnolienRealtyDbContext();

        public IEnumerable<PhotoAttachment> GetAll()
        {
            var data = _db.Photos.ToList();
            return data;
        }

        public PhotoAttachment GetById(int id)
        {
            var data = _db.Photos.FirstOrDefault(x => x.id == id);
            return data;
        }

        public void Add(PhotoAttachment data)
        {
            _db.Photos.Add(data);
            _db.SaveChanges();
        }

        public void Delete(PhotoAttachment data)
        {
            _db.Photos.Remove(data);
            _db.SaveChanges();
        }

        public void Update(PhotoAttachment data)
        {
            _db.Photos.AddOrUpdate(data);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
