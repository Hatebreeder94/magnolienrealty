﻿using System;
using System.Collections.Generic;

namespace MagnolienRealty.DAL
{
    public interface IRepository<T> : IDisposable
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Add(T data);
        void Delete(T data);
        void Update(T data);
    }
}
