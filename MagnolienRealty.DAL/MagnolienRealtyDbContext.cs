﻿using MagnolienRealty.Entity;
using System.Data.Entity;

namespace MagnolienRealty.DAL
{
    public class MagnolienRealtyDbContext : DbContext
    {
        public MagnolienRealtyDbContext() : base("DbContext") { }

        public DbSet<RealtyForSale> Realties { get; set; }
        public DbSet<PhotoAttachment> Photos { get; set; }
        public DbSet<Admin> Admins { get; set; }
    }
}
